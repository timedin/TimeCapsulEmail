# TimeCapsulEmail

TimeCapsulEmail is a web tool that allows users to send messages to their future selves using email. With TimeCapsulEmail, users can compose messages, set a delivery date, and have the message automatically delivered to their inbox at the specified time in the future.

## Features

- **Compose Messages:** Users can easily compose messages using a user-friendly interface or their normal email program.
- **Scheduled Delivery:** Messages are delivered automatically on the specified delivery date.
- **Email Integration:** Messages are sent and received using email, ensuring reliable delivery to the intended recipients.
- **Future Self Communication:** Connect with your future self by sending meaningful messages that will arrive at a designated date.

## Installation (PHP)

1. Clone the repository: 
```
git clone https://codeberg.org/timedin/TimeCapsulEmail.git
```
2. Ensure you have PHP installed on your machine.
3. Configure your web server to serve the project's files. For example, if you're using Apache, you can create a virtual host and set the document root to the cloned repository's directory.
4. Configure PHP to send emails and access the MYSQL database.
5. Setup a cronjob to execute cron.php daily.
6. Optional: Setup incoming email handling
    - Configure your email server to pipe incoming emails to the `email_receiver.php` script provided in the repository. Refer to your email server's documentation on how to set up email piping.
        - For Postfix, create an alias in `/etc/aliases`
            ```
            mailuser: "|../email_receiver.php"
            ```
            Run 
            ```
            newaliases
            ```
7. Configure the settings in the `config.ini` file.
8. Start your web server and access the web tool through the provided URL.

    - If you prefer to use a PHP development server for testing, navigate to the project's root directory and run the following command:
    ```
    php -S localhost:8000
    ```


## Usage

1. Access the web tool via the provided URL or run it locally on your machine.
2. Compose your message, specify the delivery date, and submit it.
3. Click on the link in the verification email.
4. Your message will be automatically sent to your future self at the designated date.

## Contributing

Contributions are welcome! If you find any issues or have suggestions for improvements, please feel free to open an issue or submit a pull request.

## License

This project is licensed under the [MIT License](LICENSE).

## Contact

If you have any questions, you can reach out to the project maintainer at [contact@timedin.net](mailto:contact@timedin.net).
