CREATE TABLE IF NOT EXISTS `pending_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auth_code` varchar(20) NOT NULL,
  `recipient` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `sending_date` date NOT NULL,
  `creation_date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recipient` varchar(200) NOT NULL,
  `message` text NOT NULL,
  `sending_date` date NOT NULL,
  `creation_date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
);