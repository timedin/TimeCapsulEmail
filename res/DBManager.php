<?php
/**
* 
* PHP UTILS BY TIMEDIN 
* https://codeberg.org/timedin/php-utils/
* 01.06.2023
* 
**/
class DBManager
{
	public $link;
	public $host;
	public $username;
	public $password;
	public $dbname;

	function __construct($host,$username,$password, $dbname)
	{
		if(!extension_loaded("mysqli")) {
			throw new Exception("Mysqli not installed!");
		}
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->dbname = $dbname;

		$this->connect();
	}
	public function connect()
	{
		$this->link = new mysqli($this->host, $this->username, $this->password, $this->dbname);
		// Check connection
    	if ($this->link->connect_error) {
      		die("Connection failed: " . $this->link->connect_error);
    	}
	}
	public function querySingleRow() {
		$args = func_get_args();
		$sql = $args[0];

		foreach ($args as $key => $arg) {
			if($key >= 1) {
				$arg = $this->link->real_escape_string($arg);
				$sql = str_replace("%a".($key-1), $arg, $sql);
			}
		}

		$result = $this->link->query($sql);
	  	
		if($result !== false && $result->num_rows > 0) {
			$row = $result->fetch_assoc();
		    return $row;
	    }

		return null;
	}

	public function executeQuery() {
		$args = func_get_args();
		$sql = $args[0];

		foreach ($args as $key => $arg) {
			if($key >= 1) {
				$arg = $this->link->real_escape_string($arg);
				$sql = str_replace("%a".($key-1), $arg, $sql);
			}
		}

		$result = $this->link->query($sql);
		return ($result === TRUE);
	}

    public function delete()
    {
        return $this->executeQuery(...func_get_args());
    }
    public function update()
    {
        return $this->executeQuery(...func_get_args());
    }
	public function insert() {
        return $this->executeQuery(...func_get_args());
    }
	public function query() {
		$args = func_get_args();
		$sql = $args[0];

		foreach ($args as $key => $arg) {
			if($key >= 1) {
				$arg = $this->link->real_escape_string($arg);
				$sql = str_replace("%a".($key-1), $arg, $sql);
			}
		}

		$result = $this->link->query($sql);
		$res = array();
		
		if($result !== false) {
			while($row = $result->fetch_assoc()) {
				array_push($res, $row);
			}
		}

		return $res;
	}
}
?>