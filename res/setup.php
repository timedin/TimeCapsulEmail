<?php
if(!class_exists("DBManager")) {
    require_once(__DIR__."/DBManager.php");
}

$configFile = parse_ini_file(__DIR__."/../config/config.ini",true);
$config = $configFile["Config"];
$db_config = $configFile["Database"];

$DBManager = new DBManager(
    $db_config["db_host"],
    $db_config["db_username"],
    $db_config["db_password"],
    $db_config["db_name"]
);
?>
