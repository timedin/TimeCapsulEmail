<?php
require_once(__DIR__."/../../res/setup.php");
require_once(__DIR__."/../../res/json-api-common.php");

if (isset($data["date"]) && isset($data["text"]) && isset($data["recipient"])) {
    $recipient = $data["recipient"];
    $text = $data["text"];
    $date = $data["date"];

    if (!filter_var($recipient, FILTER_VALIDATE_EMAIL)) {
        return_error("Invalid email format");
    }

    $date_parts  = explode('-', $date);
    if(count($date_parts)!=3) {
        return_error("Invalid date format");
    }
    if (!checkdate($date_parts[1], $date_parts[2], $date_parts[0])) {
        return_error("Invalid date format");
    }
    $verificationKey = createKey();
    $insertQuery = "INSERT INTO pending_posts (auth_code, recipient, message, sending_date, creation_date) 
                    VALUES (\"%a0\", \"%a1\", \"%a2\", \"%a3\", current_timestamp())";
    $DBManager->executeQuery($insertQuery, $verificationKey, $recipient, $text, $date);

    sendVerification($recipient);
    return_success();
} else {
    return_error("Missing parameters!");
}

function createKey() {
    global $DBManager;
    $existingKeys = $DBManager->query("SELECT auth_code FROM pending_posts");
    do {
        $uniqueCode = bin2hex(random_bytes(10));
    } while(in_array($uniqueCode, $existingKeys));
    return $uniqueCode;
}

function sendVerification($recipient) {
    global $verificationKey;
    global $config;
    $verificationLink = $config["verification_endpoint"] . $verificationKey;
    $expirationTime = "24 hours";
    $msg = <<<EOD
    Hey, 
    your timecapsule has been received by us.

    Please click the link below to verify your email address and schedule your capsule:
    
    Verification Link: {$verificationLink}
    Once you verify your email address, we will proceed with scheduling your capsule. 

    Please note that the verification link is only valid for {$expirationTime}.
    
    Your message is no longer editable or viewable.

    Thanks for using our services.

    Best regards,
    TimedIn
    EOD;

    $headers = <<<EOD
    From: {$config["email_from"]}
    EOD;
    mail($recipient,"Verify Your TimeCapsule Email", $msg, $headers);
}
?>