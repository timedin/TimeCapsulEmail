<?php
require_once(__DIR__."/../../res/setup.php");

$duePosts = $DBManager->query("SELECT * FROM `posts` WHERE `sending_date` <= CURRENT_TIMESTAMP");

foreach($duePosts as $post) {
    $msg = <<<EOD
    Hey, 
    Finally, it's time to receive your TimeCapsule from {$post["sending_date"]}.

    Your Message: 


    {$post["message"]}


    We hope you've enjoyed it.

    Please note that your message and personal data now have been permanently deleted from our systems.

    Thanks for using our services.

    Best regards,
    TimedIn
    EOD;

    $headers = <<<EOD
    From: {$config["email_from"]}
    EOD;
    mail($post["recipient"],"Time to Open Your TimeCapsule!",$msg,$headers);
}

$DBManager->delete("DELETE FROM `posts` WHERE `sending_date` <= CURRENT_TIMESTAMP");
?>