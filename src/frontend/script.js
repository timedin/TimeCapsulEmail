(function (window, document, apiEndpoint) {
    "use strict";
    // Retrieve DOM elements
    var submitButton = document.querySelector("#submitButton");
    var receiverEmail = document.querySelector("#recieverEmail");
    var sendingDate = document.querySelector("#sendingDate");
    var textEmail = document.querySelector("textarea#textEmail");
    var postRequest = function (url, data, callback) {
        var request = new XMLHttpRequest();
        request.open('POST', url, true);
        request.setRequestHeader('Content-type', 'application/json');
        request.onload = function () {
            if (request.status >= 200 && request.status < 300) {
                var responseData = JSON.parse(request.responseText);
                callback(responseData);
            }
            else {
                var errorMessage = (request.response && JSON.parse(request.response).description) || "Error communicating with the backend";
                callback({ state: "error", description: errorMessage });
            }
        };
        request.onerror = function () {
            callback({ state: "error", description: "Error communicating with the backend" });
        };
        request.send(JSON.stringify(data));
    };
    // Add click event listener to the submit button
    submitButton === null || submitButton === void 0 ? void 0 : submitButton.addEventListener("click", function (event) {
        event.preventDefault();
        // Check if elements are found and have the correct types
        if (receiverEmail instanceof HTMLInputElement &&
            sendingDate instanceof HTMLInputElement &&
            textEmail instanceof HTMLTextAreaElement) {
            // Get the input values
            var recipient = receiverEmail.value;
            var date = sendingDate.value;
            var message = textEmail.value;
            var data = { date: date, text: message, recipient: recipient };
            postRequest(apiEndpoint, data, function (response) {
                if (response.state !== "success") {
                    alert("Error communicating with the backend: " + response.description);
                }
                else {
                    sendingDate.value = textEmail.value = receiverEmail.value = "";
                    alert("Your Capsule has been sent. Please click the verification link in the email you've just recieved.");
                }
            });
        }
    });
})(window, document, window["apiEndpoint"]);
