(function(window, document,apiEndpoint) {
    "use strict";    
    // Retrieve DOM elements
    const submitButton = document.querySelector("#submitButton");
    const receiverEmail = document.querySelector("#recieverEmail");
    const sendingDate = document.querySelector("#sendingDate");
    const textEmail = document.querySelector("textarea#textEmail");

    interface PostData {
        date: string;
        text: string;
        recipient: string;
    }
    const postRequest = (url:string,data:PostData,callback:CallableFunction)=> {
        const request=new XMLHttpRequest();
        request.open('POST',url,true);
        request.setRequestHeader('Content-type','application/json');
        request.onload= function() {
            if (request.status >= 200 && request.status < 300) {
                const responseData = JSON.parse(request.responseText);
                callback(responseData);
            } else {
                const errorMessage = (request.response && JSON.parse(request.response).description) || "Error communicating with the backend";
                callback({ state: "error", description: errorMessage });
            }
        };
        request.onerror = function() {
            callback({ state: "error", description: "Error communicating with the backend" });
        };
        request.send(JSON.stringify(data));
    }
    // Add click event listener to the submit button
    submitButton?.addEventListener("click", (event) => {
        event.preventDefault();
        // Check if elements are found and have the correct types
        if (
            receiverEmail instanceof HTMLInputElement &&
            sendingDate instanceof HTMLInputElement &&
            textEmail instanceof HTMLTextAreaElement
        ) {
            // Get the input values
            const recipient = receiverEmail.value;
            const date = sendingDate.value;
            const message = textEmail.value;

            let data:PostData = {date:date,text:message,recipient:recipient};

            postRequest(apiEndpoint,data,(response)=>{
                if(response.state!=="success") {
                    alert("Error communicating with the backend: "+response.description);
                } else {
                    sendingDate.value=textEmail.value=receiverEmail.value=""
                    alert("Your Capsule has been sent. Please click the verification link in the email you've just recieved.");
                }
            });
        }
    });
})(window, document, window["apiEndpoint"]);